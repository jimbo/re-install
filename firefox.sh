#!/bin/bash

function instFirefox() {
    wget -O $2 $1

    firefox $2
}

function confirmInstall() {
    INSTALLED=false
    FILE="$(mktemp).xpi"
    while [ "$INSTALLED" == "false" ]
    do
        instFirefox $1 $FILE

        printf "\nDid the firefox extension install succeed succeed? (y/n)\n"
        read -n 1 k <&1
        if [ "$k" == "y" ]
        then
            INSTALLED="true"
            printf "\n"
        fi
        rm $FILE
    done
}

function installAddons() {
while read LINE 
do
   confirmInstall $LINE
done << EOF
https://addons.mozilla.org/firefox/downloads/file/4024031/facebook_container-2.3.9.xpi
https://addons.mozilla.org/firefox/downloads/file/3736912/google_container-1.5.4.xpi
https://addons.mozilla.org/firefox/downloads/file/4054938/bitwarden_password_manager-2023.1.0.xpi
https://addons.mozilla.org/firefox/downloads/file/4050735/noscript-11.4.14.xpi
https://addons.mozilla.org/firefox/downloads/file/4053171/adblock_for_youtube-0.3.6.xpi
https://addons.mozilla.org/firefox/downloads/file/4032427/return_youtube_dislikes-3.0.0.7.xpi
EOF
}


# WD=$(pwd)
# cd ~/.mozilla/firefox
# MZCONFIGS=$(find ~+ -name "prefs.js")
# cd $WD

# TODO 
# set startpage as default and firefox privacy to strict
# search engines are configured in search.json.mozlz4 
# decompression of that requires custom tools

# TODO
# restore bookmarks from somewhere into places.sqlite

# TODO
# read all values about what to install from external file

installAddons
