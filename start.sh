#!/bin/bash

# takes environment configuration path as parameter, everything is passed to ansible command

# TODO
# check presence of ansible before install

# TODO
# specify ansible version, fix ansible installation

# sudo dnf install -y pip
# sudo python -m pip install ansible

ansible-galaxy install --force -r requirements.yml

ansible-playbook --connection=local --ask-become-pass --inventory 127.0.0.1 $@

# TODO
# Convert firefox install script to ansible one
# https://docs.ansible.com/ansible/latest/playbook_guide/playbooks_prompts.html

[ "$?" == "0" ] && ./firefox.sh || exit 1

echo "Check installation succeeded and then it is recommended to reboot since new drivers may have been installed"