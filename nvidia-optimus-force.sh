#!/bin/bash

# force nvidia gpu on optimus systems
# works on xorg only - thanks nvidia

if [ -f /etc/X11/xorg.conf.d/nvidia.conf ] 
then 
    printf "Configuration already exists at /etc/X11/xorg.conf.d/nvidia.conf\nexiting...\n" 
    exit 1
fi

LINE=$(grep PrimaryGPU /usr/share/X11/xorg.conf.d/nvidia.conf)

if [ -z "$LINE" ] 
then
    # primary gpu not already defined, so add it
    while read -r 
    do
        #echo "$REPLY" | tr -d " \t"
        # drop do not edit from the file
        [ "$REPLY" == "#Do not edit" ] && continue
        echo "$REPLY" >> /etc/X11/xorg.conf.d/nvidia.conf
        # reply begins with leading tab, so drop that from match
        if [ "$(echo $REPLY | tr -d " \t")" == "Driver\"nvidia\"" ]
        then
            echo "proper place found for config option, setting.."
            printf "\tOption \"PrimaryGPU\" \"yes\"\n" >> /etc/X11/xorg.conf.d/nvidia.conf
        fi
    done </usr/share/X11/xorg.conf.d/nvidia.conf
    echo "done, reboot to xorg to test"
    exit 0
else
    echo "PrimaryGPU is already defined in the source file, this script is likely outdated, exiting"
    exit 1
fi
# cp -p /usr/share/X11/xorg.conf.d/nvidia.conf /etc/X11/xorg.conf.d/nvidia.conf

